import { Injectable } from '@angular/core';

@Injectable()
export class HeroesService {

private heroes:Heroe[] = [
    {
      nombre: "Robin",
      bio: "Dick Grayson en su niñez era un acróbata callejero, el más joven de una familia llamada The Flying Graysons (Los Graysons Voladores). En la historia del cómic, un gánster llamado Anthony Tony Zucco mató a los padres de Grayson al sabotear su trapecio y equipamientos. Batman (con la identidad de Bruce Wayne) estaba investigando el crimen y puso a Dick bajo su custodia como tutor legal. Lo entrenó física y psicológicamente para que pudiera ser su asistente. Juntos investigaron el caso de la muerte de los padres y atraparon a Zucco.",
      img: "assets/img/robin.jpg",
      aparicion: "1940-04-01",
      casa:"DC"
    },
    {
      nombre: "Batman",
      bio: "Los rasgos principales de Batman se resumen en «destreza física, habilidades deductivas y obsesión». La mayor parte de las características básicas de los cómics han variado por las diferentes interpretaciones que le han dado al personaje.",
      img: "assets/img/batman.jpg",
      aparicion: "1939-05-01",
      casa:"DC"
    },
    {
      nombre: "Flash",
      bio: "el Flash original apareció por primera vez en Flash Comics #1 (enero de 1940). Todas las encarnaciones de Flash poseen súper velocidad, que incluye la habilidad de correr y moverse extremadamente rápido, usar reflejos sobrehumanos, y aparentemente violar ciertas leyes de la física.",
      img: "assets/img/flash.jpg",
      aparicion: "1964-01-01",
      casa: "DC"
    },
    {
      nombre: "Hulk",
      bio: "Su principal poder es su capacidad de aumentar su fuerza hasta niveles prácticamente ilimitados a la vez que aumenta su furia. Dependiendo de qué personalidad de Hulk esté al mando en ese momento (el Hulk Banner es el más débil, pero lo compensa con su inteligencia).",
      img: "assets/img/hulk.jpg",
      aparicion: "1962-05-01",
      casa:"Marvel"
    },
    {
      nombre: "Flecha Verde",
      bio: "El personaje es un símil superheroico de Robin Hood. Flecha Verde es un arquero que inventa flechas de variadas funciones, tales como la pegajosa, la red, la explosiva, la bomba de tiempo, la de extinción de incendios, la flash, la de gases lacrimógenos, criogénicas, guante de boxeo e incluso una de kryptonita",
      img: "assets/img/flecha.jpg",
      aparicion: "1941-01-01",
      casa: "DC"
    },
    {
      nombre: "Spider-Man",
      bio: "Tras ser mordido por una araña radiactiva, obtuvo los siguientes poderes sobrehumanos, una gran fuerza, agilidad, poder trepar por paredes. La fuerza de Spider-Man le permite levantar 10 toneladas o más. Gracias a esta gran fuerza Spider-Man puede realizar saltos íncreibles. Un \"sentido arácnido\", que le permite saber si un peligro se cierne sobre él, antes de que suceda. En ocasiones este puede llevar a Spider-Man al origen del peligro.",
      img: "assets/img/spiderman.png",
      aparicion: "1962-08-01",
      casa: "Marvel"
    },
    {
      nombre: "Wolverine",
      bio: "En el universo ficticio de Marvel, Wolverine posee poderes regenerativos que pueden curar cualquier herida, por mortal que ésta sea, además ese mismo poder hace que sea inmune a cualquier enfermedad existente en la Tierra y algunas extraterrestres . Posee también una fuerza sobrehumana, que si bien no se compara con la de otros superhéroes como Hulk, sí sobrepasa la de cualquier humano.",
      img: "assets/img/wolverine.jpg",
      aparicion: "1974-11-01",
      casa: "Marvel"
    },
    {
      nombre: "Thor",
      bio: "Thor, cuyo nombre real es Thor Odinson conocido como Donald Blake en la tierra como su identidad secreta, es un superhéroe y príncipe-guerrero Asgardiano, el Semi-dios del Trueno, y un protector auto-proclamado de la Tierra. Thor, posteriormente, se volvió muy conocido por sus acciones en la Tierra. Thor tuvo una infancia buena, criado en Asgard como el atesorado hijo de Odín y su esposa Frigga. Su mejor amigo y compañero de juegos era su hermano adoptivo Loki y aunque eran rivales por el trono de su padre, ellos todavía permanecían como compañeros cercanos",
      img: "assets/img/thor.jpg",
      aparicion: "1962-08-01",
      casa: "Marvel"
    },
    {
      nombre: "Capitan America",
      bio: "El Capitán América viste un traje que lleva un motivo de la bandera de los Estados Unidos, y está armado con un escudo (compuesto de una aleación de un metal Extraterrestre Adamantium y de Vibranium que se encuentra en continente Africano y más específicamente en el país de Wakanda) indestructible el cual arroja a sus enemigos.",
      img: "assets/img/capi.jpg",
      aparicion: "1941-03-01",
      casa: "Marvel"
    },
    {
      nombre: "Black Widow",
      bio: "Natasha Romanoff como la Viuda Negra es una atleta y gimnasta de clase mundial, experta en artes marciales (incluyendo karate, judo, savate, varios estilos de kung fu y boxeo), tiradora y especialista en armas; así como también entrenada con un extenso entrenamiento en espionaje y es también una bailarina consumada.",
      img: "assets/img/black.jpg",
      aparicion: "1964-04-01",
      casa: "Marvel"
    },
    {
      nombre: "Ironman",
      bio: "Por encima de los poderes que Stark obtiene con su armadura, Tony es una persona con un nivel intelectual nivel 12, superior al de la escala humana y que usa constantemente para la creación de nuevas tecnologías y su renovación. Muy bien conocido en el mundo de los negocios, capaz de captar la atención de las personas cuando da discursos relacionados con economía.",
      img: "assets/img/iron.jpg",
      aparicion: "1963-03-01",
      casa: "Marvel"
    },
    {
      nombre: "Superman",
      bio:"Durante la mayor parte de su existencia, el famoso arsenal de poderes de Superman ha incluido vuelo, superfuerza, invulnerabilidad, supervelocidad, poderes de visión (rayos x, calorífica, telescópica, infrarroja, y microscópica), superoído, y superaliento, el cual le permite soplar a temperaturas congelantes así como ejercer la fuerza de un viento huracanado.",
      img: "assets/img/super.jpg",
      aparicion: "1938-06-01",
      casa: "DC"
    },
    {
      nombre: "Mujer maravilla",
      bio:"Posee una gran fuerza que fácil rivaliza la de los Kriptonianos más poderosos (Superman, Supergirl) y posee una increíble inteligencia, pues ella tiene conocimiento de magia, ciencias, sabe más de 10 idiomas y es considerada una de los miembros más sabios e inteligentes de la Liga de la Justicia. Puede volar a grandes velocidades. Es vulnerable pero es considerada a un nivel de diosa por el resto de los superhéroes ya que no envejece, es inmortal, y posee una belleza extraordinaria.",
      img: "assets/img/wonder.jpg",
      aparicion: "1941-12-01",
      casa: "DC"
    },
    {
      nombre: "Antman",
      bio:"Scott Lang era un ingeniero eléctrico y ladrón que se reformó gracias a la ayuda de Hank Pym e Iron Man. Luego de una carrera bastante larga, aunque no muy destacada, como el segundo Ant-Man, se afilió a los 4 Fantásticos y más tarde se convirtió en miembro de los Vengadores. Durante algún tiempo, mantuvo una relación romántica con Jessica Jones.",
      img: "assets/img/ant.jpg",
      aparicion: "1962-01-01",
      casa: "Marvel"
    },
    {
      nombre: "Black panther",
      bio:"Siglos atrás, cuando cinco tribus africanas luchaban por un meteorito formado de Vibranium, un metal alienígena, un guerrero-chamán ingirió una hierba en forma de corazón afectada por el metal y adquiriendo con esto habilidades sobrehumanas. Con esto, se convierte en el primer Pantera Negra y une a las cuatro tribus y así se forma la nación de Wakanda.",
      img: "assets/img/panther.jpg",
      aparicion: "1966-01-01",
      casa: "Marvel"
    },
    {
      nombre: "Falcon",
      bio:"Es un gran entrenador de aves silvestres, y ha sido entrenado en gimnasia y combate cuerpo a cuerpo por el Capitán América Redwing es un halcón de caza entrenado que responde a las órdenes verbales y mentales de Falcon para ayudarlo en la batalla contra sus adversarios.",
      img: "assets/img/nigg.jpg",
      aparicion: "1968-01-01",
      casa: "Marvel"
    },
    {
      nombre: "Doctor Strange",
      bio:"Stephen carece realmente de verdaderos Superpoderes, en su lugar posee vastos poderes místicos (teletransportación, generación de ilusiones, proyección de energía), cuyo nivel puede compararse a las de algunas entidades cósmicas. Su principal fuente es la energía mística ambiental, derivados de la Mente, el Alma y el cuerpo",
      img: "assets/img/dr.jpg",
      aparicion: "1963-01-01",
      casa: "Marvel"
    },
    {
      nombre: "Vision",
      bio:"Visión es capaz de alterar su densidad. De este modo, es capaz de volverse más ligero que el aire para poder volar, o tan denso y duro como el diamante, e incluso hacerse intangible y atravesar la materia sólida.",
      img: "assets/img/gema.jpg",
      aparicion: "1968-01-01",
      casa: "Marvel"
    },
    {
      nombre: "Deadpool",
      bio:"Conocido como el Mercenario Bocazas, es famoso por su naturaleza comunicativa y por su tendencia a romper la Cuarta pared, un recurso utilizado por los escritores para un efecto humorístico. Su mayor enemigo es Taskmaster.",
      img: "assets/img/deadpool.jpg",
      aparicion: "1991-01-01",
      casa: "Marvel"
    },
    {
      nombre: "Bucky",
      bio:"Después de haber entrenado con Steve Rogers (el Capitán América original en la Segunda Guerra Mundial) y otros en el tiempo previo a la Segunda Guerra Mundial, Bucky Barnes es un maestro del combate cuerpo a cuerpo y las artes marciales, además de ser experto en el uso de armas de guerra, tales como armas de fuego y granadas. ",
      img: "assets/img/invierno.jpg",
      aparicion: "1941-01-01",
      casa: "Marvel"
    },
      {
        nombre: "The Punisher",
        bio:"Frank Castle es un ex marine que tenía una vida ordinaria con su mujer e hijos. Él, su esposa y sus hijos fueron de paseo al Central Park, donde fueron testigos de un golpe de la mafia, por lo tanto los cuatro fueron abatidos por la mafia y él fue el único superviviente. Escapó milagrosamente con vida y juró castigar a los responsables. Desde ese momento, Frank Castle se decidió a iniciar una guerra abierta contra el crimen, utilizando para ello métodos que no siempre se encuentran dentro de la ley.",
        img: "assets/img/punisher.jpg",
        aparicion: "1974-01-01",
        casa: "Marvel"
      },
      {
        nombre: "Daredevil",
        bio:"Después de que su madre lo abandone, Matt Murdock es criado por su padre el boxeador Jack Batallador Murdock, en Hell's Kitchen (Barrio de Manhattan, Nueva York) . Al darse cuenta de que las reglas son necesarias para evitar que las personas se comporten indebidamente, el joven Matt decide estudiar Derecho. Y, al tratar de impedir un accidente, un camión derrama su carga radioactiva y ciega a Matt. Sorprendentemente, la radiación incrementa sus cuatro sentidos restantes y le otorga habilidades sobrehumanas.",
        img: "assets/img/daredevil.jpg",
        aparicion: "1964-01-01",
        casa: "Marvel"
      },
      {
        nombre: "Ciclope",
        bio:"Cíclope puede emitir potentes rayos de energía de sus ojos. No puede controlar las vigas sin la ayuda de gafas especiales que debe usar en todo momento. Él es generalmente considerado el primero de los X-Men",
        img: "assets/img/ciclope.jpg",
        aparicion: "1963-01-01",
        casa: "Marvel"
      },
      {
        nombre: "Gambito",
        bio:"Gambito puede cargar objetos inanimados con energía (energía potencial relativa a la actividad de las moléculas de un objeto), dándoles poder explosivo. Uno de sus trucos característicos es lanzar cartas (siendo su carta favorita la reina de corazones) a sus oponentes, cargando de energía cada carta",
        img: "assets/img/gambito.jpg",
        aparicion: "1990-01-01",
        casa: "Marvel"
      },
      {
        nombre: "Venom",
        bio:"Es uno de los más despiadados y peligrosos del universo Marvel y uno de los principales enemigos de Spider-Man, y ha sido tal su relevancia e impacto entre los lectores de éste que a pesar de ser lo que se conoce popularmente como un supervillano, ha protagonizado varias colecciones de cómics con su nombre en la cabecera.",
        img: "assets/img/venom.jpg",
        aparicion: "1984-01-01",
        casa: "Marvel"
      },
      {
        nombre: "Logan",
        bio:"En el universo ficticio de Marvel, Wolverine posee poderes regenerativos que pueden curar cualquier herida, por mortal que ésta sea, además ese mismo poder hace que sea inmune a cualquier enfermedad existente en la Tierra y algunas extraterrestres. Posee también una fuerza sobrehumana, que si bien no se compara con la de otros superhéroes como Hulk, sí sobrepasa la de cualquier humano.",
        img: "assets/img/logan.jpg",
        aparicion: "1974-01-01",
        casa: "Marvel"
      },
      {
        nombre: "Deathstroke",
        bio:"Como resultado de un tratamiento hormonal experimental llevado a cabo por el gobierno de EEUU Slade se convirtió en un super soldado. Sin que lo supieran los superiores de Slade, el procedimiento le aumentó su capacidad cerebral, lo que aumentaría su coordinación y resistencia sobrehumana.",
        img: "assets/img/deathstroke.jpg",
        aparicion: "1980-01-01",
        casa: "DC"
      },
      {
        nombre: "Maquina de guerra",
        bio:"Rhodes fue el ayudante de Stark en el diseño y desarrollo del traje de combate MPI-2100, una maravilla tecnológica que combinaba armadura, armamento pesado y fluidez de movimientos para las tropas de infantería. Viendo su vasto arsenal, el traje fue apropiadamente conocido como “Máquina de Guerra”.",
        img: "assets/img/maquina.jpg",
        aparicion: "1979-01-01",
        casa: "Marvel"
      },
      {
        nombre: "Thanos",
        bio:"Thanos proviene de Titán, la luna de Saturno, que dentro del Universo Marvel está habitada por los Eternos. Durante su juventud, Thanos no se interesó por la muerte hasta que la misma entidad comenzó a manipularlo desde joven. Tras sus crímenes contra los suyos en Titán, huyó del planeta buscando un amor que le correspondiera para llenar el vacío que aquella chica (la Muerte) le dejó en el corazón.",
        img: "assets/img/thanos.jpg",
        aparicion: "1979-01-01",
        casa: "Marvel"
      },
  ];




  constructor() {
console.log("Servicio listo para usar!!!");
   }


   getHeroes():Heroe[]{
     return this.heroes;
}

  getHeroe(idx: string){
    return this.heroes[idx];
      }

      buscarHeroes(termino:string): Heroe[] {
        let heroesArr:Heroe[] = [];
        termino = termino.toLowerCase();

        for (let heroe of this.heroes){

            let nombre = heroe.nombre.toLowerCase();
          if (nombre.indexOf(termino) >= 0){
          heroesArr.push(heroe)
        }
}

      return heroesArr;
    }
}


export interface Heroe{
    nombre: string;
    bio: string;
    img: string;
    aparicion: string;
    casa: string;
}
